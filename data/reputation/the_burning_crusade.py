reputations_tbc = dict(
    name='The Burning Crusade',
    key='tbc',
    reputations=[
        # 2.4 - Sunwell
        [
            dict(id=1077, name='Shattered Sun Offensive', icon='inv_shield_48'),
        ],
        # 2.1 - Black Temple
        [
            dict(id=1015, name='Netherwing', icon='ability_mount_netherdrakepurple'),
            dict(id=1038, name="Ogri'la", icon='inv_misc_apexis_crystal'),
            dict(id=1031, name="Sha'tari Skyguard", icon='ability_hunter_pet_netherray'),
        ],
        [
            dict(
                alliance_id=946,
                alliance_name='Honor Hold',
                alliance_icon='spell_misc_hellifrepvphonorholdfavor',
                horde_id=947,
                horde_name='Thrallmar',
                horde_icon='inv_misc_token_thrallmar',
            ),
            dict(
                alliance_id=978,
                alliance_name='Kurenai',
                alliance_icon='inv_misc_foot_centaur',
                horde_id=941,
                horde_name="The Mag'har",
                horde_icon='inv_misc_foot_centaur',
            ),
            dict(id=1012, name='Ashtongue Deathsworn', icon='achievement_reputation_ashtonguedeathsworn'),
            dict(id=942, name='Cenarion Expedition', icon='ability_racial_ultravision'),
            dict(id=933, name='The Consortium', icon='inv_enchant_shardprismaticlarge'),
            dict(id=989, name='Keepers of Time', icon='spell_holy_borrowedtime'),
            dict(id=990, name='The Scale of the Sands', icon='inv_enchant_dustillusion'),
            dict(id=970, name='Sporeggar', icon='inv_mushroom_11'),
            dict(id=967, name='The Violet Eye', icon='spell_holy_mindsooth'),
        ],
        [
            dict(id=932, name='The Aldor', icon='inv_misc_desecrated_plategloves'),
            dict(id=934, name='The Scryers', icon='inv_misc_book_07'),
            dict(id=1011, name='Lower City', icon='inv_feather_06'),
            dict(id=935, name="The Sha'tar", icon='inv_shield_30'),
        ],
    ],
)
