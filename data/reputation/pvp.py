reputations_pvp = dict(
    name='PvP',
    key='pvp',
    reputations=[
        # Legion 7.1 - Weird PvP traitor thing
        [
            dict(id=2018, name="Talon's Vengeance", icon='inv_falcosaurospet_white'),
        ],
        # Tol Barad
        [
            dict(
                alliance_id=1177,
                alliance_name="Baradin's Wardens",
                alliance_icon='inv_banner_tolbarad_alliance',
                horde_id=1178,
                horde_name="Hellscream's Reach",
                horde_icon='inv_banner_tolbarad_horde',
            ),
        ],
        # Vanilla battlegrounds
        [
            dict(
                alliance_id=509,
                alliance_name='The League of Arathor',
                alliance_icon='ability_warrior_rallyingcry',
                horde_id=510,
                horde_name='The Defilers',
                horde_icon='spell_shadow_psychichorrors',
            ),
            dict(
                alliance_id=730,
                alliance_name='Stormpike Guard',
                alliance_icon='inv_jewelry_stormpiketrinket_05',
                horde_id=729,
                horde_name='Frostwolf Clan',
                horde_icon='inv_jewelry_frostwolftrinket_05',
            ),
            dict(
                alliance_id=890,
                alliance_name='Silverwing Sentinels',
                alliance_icon='ability_racial_shadowmeld',
                horde_id=889,
                horde_name='Warsong Outriders',
                horde_icon='ability_warrior_warcry',
            ),
        ],
    ],
)
